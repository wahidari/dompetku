# Project DompetKu

[![](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/build.svg)](https://wahidari.gitlab.io)
[![](https://semaphoreci.com/api/v1/projects/2f1a5809-418b-4cc2-a1f4-819607579fe7/400484/shields_badge.svg)](https://wahidari.gitlab.io)
[![](https://img.shields.io/badge/docs-latest-brightgreen.svg?style=flat&maxAge=86400)](https://wahidari.gitlab.io)
[![](https://img.shields.io/badge/Find%20Me-%40wahidari-009688.svg?style=social)](https://wahidari.gitlab.io)

## Language

- [![](https://img.shields.io/badge/java-8-red.svg)](https://wahidari.gitlab.io) 
- [![](https://img.shields.io/badge/xml-1.0-green.svg)](https://wahidari.gitlab.io) 
- [![](https://img.shields.io/badge/kotlin-1.2-9C27B0.svg)](https://wahidari.gitlab.io)
- [![](https://img.shields.io/badge/sqlite-3-blue.svg)](https://wahidari.gitlab.io) 

## Screenshot

![](./ss/a.jpg)
![](./ss/b.jpg)

![](./ss/c.jpg)
![](./ss/d.jpg)

![](./ss/e.jpg)
![](./ss/f.jpg)

![](./ss/g.jpg)
![](./ss/h.jpg)

![](./ss/i.jpg)
![](./ss/j.jpg)

![](./ss/k.jpg)
![](./ss/l.jpg)

![](./ss/m.jpg)
![](./ss/n.jpg)

![](./ss/o.jpg)
![](./ss/p.jpg)

![](./ss/q.jpg)
![](./ss/r.jpg)

![](./ss/s.jpg)
![](./ss/t.jpg)

![](./ss/u.jpg)
![](./ss/v.jpg)

![](./ss/w.jpg)
